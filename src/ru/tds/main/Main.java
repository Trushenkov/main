package ru.tds.main;

import java.io.*;
import java.util.regex.*;

/**
 * Класс для работы с текстовыми файлами.
 *
 * @author Трушенков Дмитрий 15ОИТ18
 */
public class Main {
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src/ru/tds/main/input.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/ru/tds/main/output.txt"));
            BufferedWriter error = new BufferedWriter(new FileWriter("src/ru/tds/main/error.txt"));
            String string;
            try {
                while ((string = reader.readLine()) != null) {
                    if (!isCheckup(string)) {
                        error.write(string);
                        error.write("\r\n");
                        continue;
                    }
                    String[] strings = splitString(string);
                    writer.write(String.valueOf(math(strings)));
                    writer.write("\r\n");
                }

            } finally {
                reader.close();
                writer.close();
                error.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Неправильный путь к файлу.");
        }
    }

    /**
     * Метод для выполнения вычислений в зависимости от знака действия
     *
     * @param strings массив с исходными данными
     * @return результат вычисления
     */
    private static int math(String[] strings) {
        switch (strings[1]) {
            case "+":
                return summa(strings);
            case "-":
                return difference(strings);
            case "*":
                return multiplication(strings);
            case ":":
                return div(strings);
        }

        return Integer.parseInt(null);
    }

    /**
     * Метод для разбиения строки на части по пробелам
     *
     * @param string строка исходных данных
     * @return массив исходных данных
     */
    private static String[] splitString(String string) {
        String[] strings = string.split(" ");
        return strings;
    }

    /**
     * Метод для проверки строки на правильность ввода
     *
     * @param string строка исходных данных
     * @return true, если строка соответствует шаблону, false, если нет.
     */
    private static boolean isCheckup(String string) {
        Pattern p = Pattern.compile("^-?[1-9][0-9]*\\s[-:*+]\\s-?[1-9][0-9]*$");
        Matcher matcher = p.matcher(string);
        return matcher.find();
    }

    /**
     * Метод для выполнения сложения
     *
     * @param strings массив, в котором хранятся исходные данные
     * @return
     */
    public static int summa(String[] strings) {
        return Integer.parseInt(strings[0]) + Integer.parseInt(strings[2]);
    }

    /**
     * Метод для выполнения вычитания
     *
     * @param strings массив, в котором хранятся исходные данные
     * @return
     */
    public static int difference(String[] strings) {
        return Integer.parseInt(strings[0]) - Integer.parseInt(strings[2]);
    }

    /**
     * Метод для выполнения деления
     *
     * @param strings массив, в котором хранятся исходные данные
     * @return
     */
    public static int div(String[] strings) {
        return Integer.parseInt(strings[0]) / Integer.parseInt(strings[2]);
    }

    /**
     * Метод для выполнения умножения
     *
     * @param strings массив, в котором хранятся исходные данные
     * @return
     */
    public static int multiplication(String[] strings) {
        return Integer.parseInt(strings[0]) * Integer.parseInt(strings[2]);
    }
}
